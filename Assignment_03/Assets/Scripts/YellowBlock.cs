﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowBlock : Block
{
    public new int ColorNum = 3;
    public override int ColorCheck()
    {
        return ColorNum;
    }
}
