﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedProjectile : Projectile
{
    public new int colorVal = 2;
    protected override void SetMovement()
    {
        directionAngle = -90;
        speed = 5;
    }
    public override int ColorCheck()
    {
        return colorVal;
    }
}
