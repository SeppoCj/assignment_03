﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour, IInteraction
{
    protected Vector3 spawnPos;
    protected int ColorNum;
    private Vector3 mOffset;
    private float mZCoord;

    protected void Update()
    {
        if (GetComponent<Rigidbody>().position.y < -5) {
            OnHit(ColorNum);
        }
    }

    protected void Start()
    {
        spawnPos = transform.position;
    }

    public virtual int ColorCheck()
    {
        return ColorNum;
    }

    public void OnHit(int colorVal)
    {
        transform.position = spawnPos;
        this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);



    }
    void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
    }

    void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = mZCoord;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.GetComponent<IInteraction>().ColorCheck());
        if (collision.gameObject.GetComponent<IInteraction>().ColorCheck() == ColorCheck())
        {
            collision.gameObject.GetComponent<IInteraction>().OnHit(ColorCheck());
            OnHit(ColorCheck());
        }
        else
        {
            OnHit(ColorCheck());
        }
    }
    
}
