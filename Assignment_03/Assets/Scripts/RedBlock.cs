﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBlock : Block
{
    public new int ColorNum = 2;

    public override int ColorCheck()
    {
        return ColorNum;
    }
}
