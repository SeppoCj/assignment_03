﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowProjectile : Projectile
{
    public new int colorVal = 3;
    protected override void SetMovement()
    {
        directionAngle = -90;
        speed = 5;
    }
    public override int ColorCheck()
    {
        return colorVal;
    }
}
