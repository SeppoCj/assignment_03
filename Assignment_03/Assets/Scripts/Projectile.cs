﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour, IInteraction
{
    private Transform m_transform;
    private Vector3 m_spawnPoint;
    private Rigidbody m_rb;

    protected int colorVal;

    protected float directionAngle;
    protected float speed;

    protected virtual void Move()
    {
        Vector3 currentPos = new Vector3(transform.position.x + Mathf.Cos(Mathf.Deg2Rad * directionAngle), transform.position.y + Mathf.Sin(Mathf.Deg2Rad * directionAngle) * speed * Time.deltaTime, 0);
        m_rb.MovePosition(currentPos);
    }

    private void Start()
    {
        m_rb = GetComponent<Rigidbody>();
        m_transform = transform;
        m_spawnPoint = transform.position;
        SetMovement();
    }

    private void Update()
    {
        Move();

        if (transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }
    }

    protected virtual void SetMovement()
    {

    }

    public void OnHit(int colorVal)
    {
       Destroy(this.gameObject);
    }

    public virtual int ColorCheck()
    {
        return colorVal;
    }
}