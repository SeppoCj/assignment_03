﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueBlock : Block
{
    public new int ColorNum = 1;
    public override int ColorCheck()
    {
        return ColorNum;
    }
}
