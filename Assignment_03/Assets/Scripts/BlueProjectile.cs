﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueProjectile : Projectile
{
    public new int colorVal = 1;
    protected override void SetMovement()
    {
        directionAngle = -90;
        speed = 5;
    }

    public override int ColorCheck()
    {
        return colorVal;
    }

}
