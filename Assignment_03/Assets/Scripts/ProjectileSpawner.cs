﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{

    public GameObject yellowBullet, redBullet, blueBullet;
    public float spawnDelay = 2;

    void Start()
    {
        StartCoroutine(SpawnObject());
    }

    private void Update()
    {
        
        if (spawnDelay > 0.4f) {
            spawnDelay -= Time.deltaTime * Time.deltaTime;
        }
    }

    public IEnumerator SpawnObject() {
        yield return new WaitForSecondsRealtime(spawnDelay);
        int temp = Random.Range(1, 4);
        switch (temp) {
            case 1:
                Instantiate(blueBullet, new Vector3(Random.Range(-8, 8), 6, 0), Quaternion.identity);
                break;
            case 2:
                Instantiate(redBullet, new Vector3(Random.Range(-8, 8), 6, 0), Quaternion.identity);
                break;
            case 3:
                Instantiate(yellowBullet, new Vector3(Random.Range(-8, 8), 6, 0), Quaternion.identity);
                break;

        }
        StartCoroutine(SpawnObject());
    }
}
