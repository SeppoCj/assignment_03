﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteraction 
{
    void OnHit(int colorVal);
    int ColorCheck();


}
